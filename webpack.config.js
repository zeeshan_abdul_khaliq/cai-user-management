const webpack = require("webpack");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    publicPath: "http://localhost:4201/",
    uniqueName: "auth",
  },
  optimization: {
    runtimeChunk: false,
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "auth",
      library: { type: "var", name: "auth" },
      filename: "remoteEntry.js",
      exposes: {
        AuthModule: "./src/app/auth/auth.module.ts",
        AdminPanelModule: "./src/app/adminPanel/adminPanel.module.ts",
      },
      shared: {
        "@angular/core": { singleton: true, requiredVersion:'12.0.1'  },
        "@angular/common": { singleton: true, requiredVersion:'12.0.1'  },
        "@angular/router": { singleton: true, requiredVersion:'12.0.1'  },
      },
    }),
  ],
};
