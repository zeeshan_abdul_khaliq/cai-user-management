import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];
  role: string;
  checkIfdisabeldRoles = true;
  constructor() {
    this.role = sessionStorage.getItem('role');
    if (this.role === 'Master Soc Analyst' || this.role === 'Distributor Soc Analyst' || this.role === 'Partner Soc Analyst') {
      this.checkIfdisabeldRoles = false;
    }
  }

  ngOnInit(): void {

    this.items = [
      {
          label: 'Home',
          routerLink: '/cai/home',
      },
      {
        label: 'Threat Intelligence Platform',
        styleClass: 'modules tip',
        icon: 'pi',
        routerLink: '/tip/dashboard',

      },
      {
        label: 'Threat Sharing Community',
        styleClass: 'modules tsc',
        icon: 'pi',
        routerLink: '/community/dashboard',

      },
      {
        label: 'Advanced Persistent Threats',
        styleClass: 'modules',
        routerLink: '/apt/dashboard',

      },
      {
        label: 'Infrastructure Monitoring',
        routerLink: '/networkMon/dashboard',
    },
    {
      label: 'Vulnerability Prioritization',
      styleClass: 'modules',
      routerLink: '/vp/dashboard',

    },
  {
    label: 'Attack Surface Monitoring',
    styleClass: 'modules'
  },
  {
    label: 'Distributed Deception Platform',
    styleClass: 'modules',
  },
  {
    label: 'SIEM',
    styleClass: 'modules',
  },
  {
    label: 'SOAR',
  },

  {
    label: 'Third Party Vendors',
    styleClass: 'modules',
    items: [
      {
        label: 'Skurio - DRP ',
        routerLink: '#',
      },
      {
        label: 'RiskXchange - ASM',
        routerLink: '#',
      }

   ]
  },

  ];

  }

}
