import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../../core-services/user-management.service';

@Component({
  selector: 'app-create-soc',
  templateUrl: './create-soc.component.html',
  styleUrls: ['./create-soc.component.scss']
})

export class CreateSocComponent implements OnInit {
  createSOC: FormGroup;
  email: string;
  submitValidation: boolean;
  roles: string[];
  additionalRoles: string[];
  constructor(private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService, private userManagementService: UserManagementService) {
    this.submitValidation = false;
    this.roles = [];
    this.additionalRoles = [];
    this.createSOC = this.formBuilder.group({
      companyName: [sessionStorage.getItem('organizationName')],
      email: ['', [Validators.required, Validators.email]],
      role: ['', [Validators.required]],
      additionalRoles: this.formBuilder.array([]),
      username: ['']
    });
  }
  ngOnInit(): void {
    this.lookUpServices();
  }
  get createSOCControl(): any {
    return this.createSOC.controls;
  }
  onCreateSOCSubmit(): void {
    if (this.createSOC.invalid) {
      this.submitValidation = true;
      return;
    }
    this.createSOC.value.username = this.createSOC.value.email;
    const createSocSubmitValues = {
        userCreateRequestDtos: [this.createSOC.value]
    };
    this.spinnerService.show('createSOC-spinner');
    this.userManagementService.addUser(createSocSubmitValues).subscribe(
        updateOrganizationalUserResponse => {
          this.spinnerService.hide('createSOC-spinner');
          this.toastrService.success('Invite has been sent to the user');
          this.router.navigate(['/admin-panel/manageUsers'],
              {queryParams: {primaryTabIndex: 1, secondaryTabIndex: 2}});
        }
        ,
        updateOrganizationalUserError => {
          this.spinnerService.hide('createSOC-spinner');
          this.toastrService.error('Something is missing in form.');
        });
  }
  cancel(): void {
    this.router.navigate(['/admin-panel/manageUsers'],
        {queryParams: {primaryTabIndex: 1, secondaryTabIndex: 0}});
  }
  lookUpServices(): void {
    this.userManagementService.lookupForPrimaryRoles('true').subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.roles = lookupForPrimaryRolesResponse;
          }
        });
    this.userManagementService.lookupForSocAdditionalRoles().subscribe(
        lookupForSecondaryRolesResponse => {
            if (lookupForSecondaryRolesResponse) {
                this.additionalRoles = lookupForSecondaryRolesResponse;
            }
        });
  }
    onCheckboxChange(additionalRoleValue): any {
        const additionRoleControl = this.createSOC.controls.additionalRoles as FormArray;

        if (additionalRoleValue.target.checked) {
            additionRoleControl.push(new FormControl(additionalRoleValue.target.value));
        } else {
            let additionRoleIndex = 0;
            additionRoleControl.controls.forEach((item) => {
                if (item.value === additionalRoleValue.target.value) {
                    additionRoleControl.removeAt(additionRoleIndex);
                    return;
                }
                additionRoleIndex++;
            });
        }
    }
}
