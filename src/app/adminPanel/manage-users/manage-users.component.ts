import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../core-services/user-management.service';
import {ActiveUser, DisabledUser, GetUserList, InvitedUser, OrganizationUserListDto, SocUserListDto} from '../../core-services/userManagements-interfaces';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ViewSocDetailsComponent} from './viewSocDetails/viewSocDetails.component';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss'],
  providers: [DialogService, DynamicDialogRef]

})
export class ManageUsersComponent implements OnInit, OnDestroy {
  organizationalAndSocUserListResponse: GetUserList;
  organizationUserListDto: OrganizationUserListDto;
  socUserListDto: SocUserListDto;
  activeUsers: ActiveUser[];
  disabledUsers: DisabledUser[];
  invitedUsers: InvitedUser[];
  userListArray: ActiveUser[];
  primaryTabIndex: number;
  secondaryTabIndex: number;
  filter$: Observable<any>;
  socEnabled = false;
  role: string;
  statusId: string;
  userStatus: string;
  showReassignedDialog = false;
  assignedSocForm: FormGroup;
  roles: ActiveUser[];
  constructor(private spinner: NgxSpinnerService, private router: Router, private toastrService: ToastrService,
              public dialogService: DialogService, private viewSoc: DynamicDialogRef,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute, private userManagementService: UserManagementService) {}
  ngOnInit(): void {
    this.primaryTabIndex = 0;
    this.secondaryTabIndex = 0;
    this.role = sessionStorage.getItem('role');
    this.roles = [];
    this.assignedSocForm = this.formBuilder.group({
      roles: ['', [Validators.required]],
    });
    this.filter$ = this.route.queryParamMap.pipe(
        map((magicLinkParam: ParamMap) => magicLinkParam.get('primaryTabIndex')),
      );
    this.filter$.subscribe(param => {
      this.primaryTabIndex = +this.route.snapshot.queryParamMap.get('primaryTabIndex');
      this.secondaryTabIndex = +this.route.snapshot.queryParamMap.get('secondaryTabIndex');
      this.changePrimaryTab({index: this.primaryTabIndex}, this.secondaryTabIndex);
      this.changeSecondaryTab({index: this.secondaryTabIndex});
    });
    this.getOrganizationalAndSocUserList();

  }
  get assignedSocFormControl(): any {
    return this.assignedSocForm.controls;
  }
  resendRoleAccess(indexRole: string): boolean {
    if ((this.role === 'Master Admin' && indexRole === 'Master Soc Analyst') ||
        (this.role === 'Distributor Admin' && indexRole === 'Distributor Soc Analyst') ||
        (this.role === 'Partner Admin' && indexRole === 'Partner Soc Analyst')) {
      return false;
    }
    return true;
  }
  editRoleAccess(): boolean {
    if (this.role === 'Master Analyst' ||  this.role === 'Distributor Analyst' || this.role === 'Partner Analyst') {
      return false;
    }
    return true;
  }
  getOrganizationalAndSocUserList(): void {
    this.activeUsers = [];
    this.disabledUsers = [];
    this.invitedUsers = [];
    this.userListArray = [];
    this.spinner.show('usersList-spinner');

    this.userManagementService.getOrganizationalAndSocUserList().subscribe(
        getOrganizationalAndSocUserListResponse => {
          this.spinner.hide('usersList-spinner');
          this.organizationalAndSocUserListResponse = getOrganizationalAndSocUserListResponse;
          if (this.organizationalAndSocUserListResponse) {
            this.organizationUserListDto = this.organizationalAndSocUserListResponse.organizationUserListDto;
            this.socUserListDto = this.organizationalAndSocUserListResponse.socUserListDto;
            this.socEnabled = this.organizationalAndSocUserListResponse.soc;
            if (this.socEnabled === true) {
              this.primaryTabIndex = 1;
            } else if (this.role === 'Client Admin'){
              this.socEnabled = true;
              this.primaryTabIndex = 0;
            }
            this.checkUserTab();
          }
        }, getOrganizationalAndSocUserListError => {
          this.spinner.hide('usersList-spinner');
        });
  }
  changePrimaryTab(primaryEvent, secondaryTabIndex): void {
    this.primaryTabIndex = primaryEvent.index;
    this.secondaryTabIndex = secondaryTabIndex;
    this.checkUserTab();
  }
  changeSecondaryTab(secondaryEvent): void {
    this.secondaryTabIndex = secondaryEvent.index;
    this.checkUserTab();
  }
  checkUserTab(): void {
    this.activeUsers = [];
    this.disabledUsers = [];
    this.invitedUsers = [];
    this.userListArray = [];
    if (this.organizationUserListDto || this.socUserListDto ) {
      switch (this.primaryTabIndex) {
        case 0:
          switch (this.secondaryTabIndex) {
            case 0 :
              this.activeUsers = this.organizationUserListDto.activeUsers;
              this.userListArray = [...this.activeUsers];
              break;
            case 1 :
              this.disabledUsers = this.organizationUserListDto.disabledUsers;
              this.userListArray = [...this.disabledUsers];
              break;
            case 2 :
              this.invitedUsers = this.organizationUserListDto.invitedUsers;
              this.userListArray = [...this.invitedUsers];
              break;
          }
          break;
        case 1:
          switch (this.secondaryTabIndex) {
            case 0 :
              this.activeUsers = this.socUserListDto.activeUsers;
              this.userListArray = [...this.activeUsers];
              break;
            case 1 :
              this.disabledUsers = this.socUserListDto.disabledUsers;
              this.userListArray = [...this.disabledUsers];
              break;
            case 2 :
              this.invitedUsers = this.socUserListDto.invitedUsers;
              this.userListArray = [...this.invitedUsers];
              break;
          }
          break;
      }

    }
  }
  checkStatusOfUser(userId, userStatus): void {
    this.statusId = userId;
    this.userStatus = userStatus;
    if (userStatus) {
      this.changeStatusOfUser(userId, userStatus);
    } else {
      this.showReassignedDialog = true;
      this.roles = [];
    }
  }
  changeStatusOfUser(userId, userStatus): void {
    this.spinner.show('usersList-spinner');
    this.userManagementService.changeStatusOfUser(userId, userStatus).subscribe(
        checkStatusOfUserResponse => {
          this.spinner.hide('usersList-spinner');
          if (userStatus) {
            this.toastrService.success('User Enabled Successfully');
            this.secondaryTabIndex = 0;
          } else {
            this.toastrService.success('User Disabled Successfully');
            this.secondaryTabIndex = 1;
          }
          this.getOrganizationalAndSocUserList();
          }, checkStatusOfUserError => {
          this.spinner.hide('usersList-spinner');
          this.toastrService.error('Some error occurred while updating status of this user');
        });
  }
  deleteOrganizationalUser(userId): void {
    this.spinner.show('usersList-spinner');
    this.userManagementService.deleteOrganizationalUser(userId).subscribe(
        deleteOrganizationalUserResponse => {
          this.spinner.hide('usersList-spinner');
          this.toastrService.success('User Deleted Successfully');
          this.getOrganizationalAndSocUserList();
        }, deleteOrganizationalUserError => {
          this.spinner.hide('usersList-spinner');
          this.toastrService.error('Some error occurred while deleting this user');
        });
  }
  resendEmail(userId): void {
    this.spinner.show('usersList-spinner');
    this.userManagementService.resendEmail(userId).subscribe(
        resendEmailResponse => {
          this.spinner.hide('usersList-spinner');
          this.toastrService.success('Email Sent Successfully');
        }, resendEmailError => {
          this.spinner.hide('usersList-spinner');
          this.toastrService.error(resendEmailError.error.responseMsg);
        });
  }
  updateOrganizationalUser(emailId): void {
    let socRole = false;
    if (this.primaryTabIndex === 1) {
      socRole = true;
    }
    this.router.navigate(['/admin-panel/updateUser'], {queryParams: {
      email: emailId, soc : socRole
      }});
  }
  viewSocDetails(userId): void {
    this.viewSoc =  this.dialogService.open(ViewSocDetailsComponent, {
      width: '95%',
      contentStyle: { overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {userId}
    });
    this.viewSoc.onClose.subscribe((response: any) => {});
  }
  ngOnDestroy(): void {
    this.viewSoc.close();
  }
  UnassignSoc(): void {
    if (this.assignedSocForm.invalid) {
      this.toastrService.error('Please select the role');
      return;
    }
    const roles = this.socUserListDto.activeUsers.filter(item => (item.emailId === this.assignedSocForm.value.roles));
    const assignedSocFormRequestDto = {
      userId: roles[0].userId,
      roles: [roles[0].role],
    };
    this.spinner.show('usersList-spinner');
    this.userManagementService.updateOrganizationalUser(assignedSocFormRequestDto).subscribe(
        viewSocDetailsResponse => {
          this.spinner.show('usersList-spinner');
          this.toastrService.success('Soc User Re-assigned Successfully!');
          this.cancel();
          this.changeStatusOfUser(this.statusId, this.userStatus);
        }, viewSocDetailsError => {
          this.spinner.show('usersList-spinner');
          this.toastrService.error(viewSocDetailsError.error.responseMsg);

        });
  }
  cancel(): void {
    this.showReassignedDialog = false;
  }
}
