import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, FormArray, AbstractControl} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../../core-services/user-management.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})

export class CreateUserComponent implements OnInit {

  createUser: FormGroup;
  email: string;
  role: string;
  organizationName: string;
  checkIfdisabeldRoles = false;
  submitValidation: boolean;
  roles: string[];
  additionalRoles: string[];
  constructor(private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService, private userManagementService: UserManagementService) {
    this.submitValidation = false;
    this.roles = [];
    this.role = sessionStorage.getItem('role');
    this.organizationName = sessionStorage.getItem('organizationName');
    if (this.role === 'Master Soc Admin' || this.role === 'Distributor Soc Admin' || this.role === 'Partner Soc Admin') {
      this.checkIfdisabeldRoles = true;
    }
    this.additionalRoles = [];
    this.createUser = this.formBuilder.group({
      userCreateRequestDtos: this.formBuilder.array([this.createForm()])
    });
  }
  createForm(): FormGroup {
    return this.formBuilder.group({
      companyName: [sessionStorage.getItem('organizationName')],
      email: ['', [Validators.required, Validators.email]],
      role: ['', [Validators.required]],
      additionalRoles: this.formBuilder.array([]),
    });
  }
  onCheckboxChange(additionalRoleValue, createFormUnit): any {
    const additionRoleControl = createFormUnit.controls.additionalRoles as FormArray;

    if (additionalRoleValue.target.checked) {
      additionRoleControl.push(new FormControl(additionalRoleValue.target.value));
    } else {
      let additionRoleIndex = 0;
      additionRoleControl.controls.forEach((item) => {
        if (item.value === additionalRoleValue.target.value) {
          additionRoleControl.removeAt(additionRoleIndex);
          return;
        }
        additionRoleIndex++;
      });
    }
  }
  addUser(): void {
    const createUserControl = this.createUser.controls.userCreateRequestDtos as FormArray;
    createUserControl.push(this.createForm());
  }
  removeUser(createUserFormIndex: number): void {
    const createUserControl = this.createUser.controls.userCreateRequestDtos as FormArray;
    createUserControl.removeAt(createUserFormIndex);
  }
  ngOnInit(): void {
    this.lookUpServices();
  }
  lookUpServices(): void {
    this.userManagementService.lookupForPrimaryRoles('false').subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.roles = lookupForPrimaryRolesResponse;
          }
        });
    this.userManagementService.lookupForAdditionalRoles().subscribe(
        lookupForSecondaryRolesResponse => {
          if (lookupForSecondaryRolesResponse) {
            this.additionalRoles = lookupForSecondaryRolesResponse;
          }
        });
  }
  onCreateUserSubmit(): void {
    if (this.createUser.invalid) {
      this.submitValidation = true;
      this.toastrService.error('Please fill the form correctly.')
      return;
    }
    this.spinnerService.show('editProfile-spinner');
    this.userManagementService.createOrganizationalUser(this.createUser.value).subscribe(
        createOrganizationalUserResponse => {
          this.spinnerService.hide('editProfile-spinner');
          this.toastrService.success('User Created Successfully');
          this.router.navigate(['/admin-panel/manageUsers'],
              {queryParams: {primaryTabIndex: 0, secondaryTabIndex: 2}});
        }
        ,
        createOrganizationalUserError => {
          this.spinnerService.hide('editProfile-spinner');
          this.toastrService.error('Something is missing in form.');
        });
  }
  cancel(): void {
    this.router.navigate(['/admin-panel/manageUsers'],
        {queryParams: {primaryTabIndex: 0, secondaryTabIndex: 0}});
  }
}
