import {Component, OnDestroy, OnInit} from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../../core-services/user-management.service';
import {ViewSocDetails} from '../../../core-services/userManagements-interfaces';
import {consoleLog} from 'echarts/types/src/util/log';

@Component({
    selector: 'app-viewsocdetails',
    templateUrl: './viewSocDetails.component.html',
    styleUrls: ['./viewSocDetails.component.scss'],
    providers: [DynamicDialogRef]
})
export class ViewSocDetailsComponent implements OnInit, OnDestroy {
    userId: string;
    socDetails: ViewSocDetails;
    role: string;
    constructor(public dynamicDialogConfig: DynamicDialogConfig, private spinnerService: NgxSpinnerService,
                private viewSocReference: DynamicDialogRef,
                private toastrService: ToastrService, private userManagementService: UserManagementService) { }

    ngOnInit(): void {
        this.userId = this.dynamicDialogConfig.data.userId;
        this.role = sessionStorage.getItem('role');
        this.getSocDetails();
    }
    getSocDetails(): void {
        this.spinnerService.show('socDetails-spinner');
        this.userManagementService.viewSocDetails(this.userId).subscribe(
            viewSocDetailsResponse => {
                this.spinnerService.hide('socDetails-spinner');
                this.socDetails = viewSocDetailsResponse;
            }, viewSocDetailsError => {
                this.spinnerService.hide('socDetails-spinner');
            });
    }
    ngOnDestroy(): void {
        this.viewSocReference.close();
    }

    UnassignSoc(userId): void {
        this.spinnerService.show('socDetails-spinner');
        this.userManagementService.UnassignSoc(this.userId, userId).subscribe(
            viewSocDetailsResponse => {
                this.spinnerService.hide('socDetails-spinner');
                this.toastrService.success('Soc User Un-assigned Successfully!')
                this.getSocDetails();
            }, viewSocDetailsError => {
                this.spinnerService.hide('socDetails-spinner');
                this.toastrService.error(viewSocDetailsError.error.responseMsg);

            });
    }
}
