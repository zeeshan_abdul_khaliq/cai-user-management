import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../../core-services/user-management.service';
@Component({
  selector: 'app-update-organizationaluser',
  templateUrl: './update-organizationalUser.component.html',
  styleUrls: ['./update-organizationalUser.component.scss']
})

export class UpdateOrganizationalUserComponent implements OnInit {

  updateUser: FormGroup;
  filter$: Observable<any>;
  email: string;
  role: string;
  checkIfdisabeldRoles = false;
  submitValidation: boolean;
  roles: string[];
  additionalRoles: string[];
  soc = 'false';
  constructor(private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService, private userManagementService: UserManagementService) {
    this.submitValidation = false;
    this.roles = [];
    this.role = sessionStorage.getItem('role');
    if (this.role === 'Master Soc Admin' || this.role === 'Distributor Soc Admin' || this.role === 'Partner Soc Admin') {
        this.checkIfdisabeldRoles = true;
    }
    this.additionalRoles = [];
    this.updateUser = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      displayName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      email: ['', [Validators.required]],
      roles: ['', [Validators.required]],
        additionalRoles: this.formBuilder.array([]),
      userId: [''],
      enabled: [true]
    });
   }
  ngOnInit(): void {
    this.filter$ = this.route.queryParamMap.pipe(
        map((magicLinkParam: ParamMap) => magicLinkParam.get('email')),
    );
    this.filter$.subscribe(param => {
      this.email = this.route.snapshot.queryParamMap.get('email');
      this.soc = this.route.snapshot.queryParamMap.get('soc');
      this.getOrganizationalUser();
    });
    this.lookUpServices();
  }
  getOrganizationalUser(): void {
    this.spinnerService.show('editProfile-spinner');
    this.userManagementService.getOrganizationalUser(this.email).subscribe(
        getOrganizationalUserResponse => {
          this.spinnerService.hide('editProfile-spinner');
          if (getOrganizationalUserResponse) {
            this.updateUser.patchValue({
              firstName: getOrganizationalUserResponse.firstName,
              lastName: getOrganizationalUserResponse.lastName,
              displayName: getOrganizationalUserResponse.displayName,
              phoneNumber: getOrganizationalUserResponse.phoneNumber,
              email: this.email,
              roles: getOrganizationalUserResponse.role,
              additionalRoles: getOrganizationalUserResponse.additionalRoles,
              userId: getOrganizationalUserResponse.userId,
            });
          }
        }, getOrganizationalUserError => {
          this.spinnerService.hide('editProfile-spinner');
        });
  }
  lookUpServices(): void {
    this.userManagementService.lookupForPrimaryRoles(this.soc).subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.roles = lookupForPrimaryRolesResponse;
          }
        });
    this.userManagementService.lookupForAdditionalRoles().subscribe(
        lookupForSecondaryRolesResponse => {
            if (lookupForSecondaryRolesResponse) {
                this.additionalRoles = lookupForSecondaryRolesResponse;
            }
        });
  }
   get updateUserControl(): any {
    return this.updateUser.controls;
  }
  onUpdateUserSubmit(): void {
    if (this.updateUser.invalid) {
      this.submitValidation = true;
      return;
    }
    this.updateUser.value.roles = [this.updateUser.value.roles];
    this.spinnerService.show('editProfile-spinner');
    this.userManagementService.updateOrganizationalUser(this.updateUser.value).subscribe(
        updateOrganizationalUserResponse => {
          this.spinnerService.hide('editProfile-spinner');
          this.toastrService.success('User Updated Successfully');
          this.router.navigate(['/admin-panel/manageUsers'],
                {queryParams: {primaryTabIndex: 0, secondaryTabIndex: 0}});
          }
        ,
        updateOrganizationalUserError => {
          this.spinnerService.hide('editProfile-spinner');
          this.toastrService.error('Something is missing in form.');
        });
  }
  cancel(): void {
      this.router.navigate(['/admin-panel/manageUsers'],
          {queryParams: {primaryTabIndex: 0, secondaryTabIndex: 0}});
  }
  setAdditionalRoleControl(abstractControl: AbstractControl): FormControl {
    const additionalRoleControl = abstractControl as FormControl;
    return additionalRoleControl;
  }
  onCheckboxChange(additionalRoleValue): any {
      const additionRoleControl = this.updateUser.controls.additionalRoles as FormArray;
      if (additionalRoleValue.target.checked) {
          additionRoleControl.push(new FormControl(additionalRoleValue.target.value));
      } else {
          let additionRoleIndex = 0;
          additionRoleControl.controls.forEach((item) => {
              if (item.value === additionalRoleValue.target.value) {
                  additionRoleControl.removeAt(additionRoleIndex);
                  return;
              }
              additionRoleIndex++;
          });
      }
  }
}
