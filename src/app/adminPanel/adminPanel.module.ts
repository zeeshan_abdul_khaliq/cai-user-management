import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { AdminPanelRoutingModule } from './adminPanel-routing.module';

import {InputTextModule} from 'primeng/inputtext';
import {FormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ComponentsModule} from '../framework/components/components.module';
import {ProfileComponent} from './profile/profile.component';
import {ManageUsersComponent} from './manage-users/manage-users.component';
import {AdminComponent} from './admin/admin.component';
import {TabViewModule} from 'primeng/tabview';
import {TableModule} from 'primeng/table';
import {CardModule} from 'primeng/card';
import {AdminPanelComponent} from './adminPanel.component';
import {CoreServicesModule} from '../core-services/core-services.module';
import {AuthGuard} from '../core-services/auth.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from '../core-services/token.interceptor.service';
import {ToastModule} from 'primeng/toast';
import { CreateUserComponent } from './manage-users/create-user/create-user.component';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {TagModule} from 'primeng/tag';
import {TooltipModule} from 'primeng/tooltip';
import {CreateSocComponent} from './manage-users/create-soc/create-soc.component';
import {UpdateOrganizationalUserComponent} from './manage-users/update-organizationalUser/update-organizationalUser.component';
import {CheckboxModule} from 'primeng/checkbox';
import {PasswordModule} from 'primeng/password';
import {ViewSocDetailsComponent} from './manage-users/viewSocDetails/viewSocDetails.component';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import { CreateCompanyComponent } from './admin/create-company/create-company.component';
import {CalendarModule} from 'primeng/calendar';
import { ChipModule } from 'primeng/chip';
import {UpdateCompanyComponent} from './admin/update-company/update-company.component';
import {DialogModule} from 'primeng/dialog';

@NgModule({
  declarations: [
    AdminPanelComponent,
    ProfileComponent,
    ManageUsersComponent,
    AdminComponent,
    CreateUserComponent,
    CreateSocComponent,
    UpdateOrganizationalUserComponent,
    ViewSocDetailsComponent,
    CreateCompanyComponent,
    UpdateCompanyComponent
  ],
  imports: [
    CommonModule,
    AdminPanelRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    ConfirmDialogModule,
    InputTextModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    TabViewModule,
    CardModule,
    TableModule,
    CoreServicesModule,
    ToastModule,
    DropdownModule,
    MultiSelectModule,
    ToastrModule.forRoot(),
    TagModule,
    TooltipModule,
    CheckboxModule,
    PasswordModule,
    DynamicDialogModule,
    ChipModule,
    CalendarModule,
    DialogModule
  ],
  providers: [AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true,
  },
    ToastrService]
})
export class AdminPanelModule { }
