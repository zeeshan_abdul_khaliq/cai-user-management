import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {UserManagementService} from '../../core-services/user-management.service';
import {ConfirmedValidator} from '../../auth/register/confirmed.validator';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],

})
export class ProfileComponent implements OnInit {
    editProfileForm: FormGroup;
    changePasswordForm: FormGroup;
    emailId: string;
    constructor(private formBuilder: FormBuilder, public spinnerService: NgxSpinnerService,
                private toastrService: ToastrService, private router: Router,
                private userManagementService: UserManagementService) {
        this.editProfileForm = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
            lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
            displayName: ['', [Validators.required]],
            phoneNumber: ['', [Validators.required]],
            userId: [''],
        });
        this.changePasswordForm = this.formBuilder.group({
            password: ['', [Validators.required]],
            oldPassword: ['', [Validators.required]],
            confirmPassword: ['', [Validators.required]],
        }, {
            validator: ConfirmedValidator('password', 'confirmPassword')
        });
    }

    get errorControl(): any {
        return this.editProfileForm.controls;
    }
    get changePasswordControl(): any {
        return this.changePasswordForm.controls;
    }

    ngOnInit(): void {
        this.spinnerService.show('editProfile-spinner');
        this.emailId = sessionStorage.getItem('username');
        this.userManagementService.getProfile(this.emailId).subscribe(
            getProfileResponse => {
                if (getProfileResponse) {
                    this.editProfileForm.patchValue({
                        firstName: getProfileResponse.firstName,
                        lastName: getProfileResponse.lastName,
                        displayName: getProfileResponse.displayName,
                        phoneNumber: getProfileResponse.phoneNumber,
                        userId: getProfileResponse.userId
                    });
                }
                this.spinnerService.hide('editProfile-spinner');
            }, getProfileError => {
                this.spinnerService.hide('editProfile-spinner');
            }
        );
    }
    updateProfile(): void {
        if (this.editProfileForm.invalid) {
            return;
        }
        this.spinnerService.show('editProfile-spinner');
        this.userManagementService.updateProfile(this.editProfileForm.value).subscribe(
            updateProfileResponse => {
                this.spinnerService.hide('editProfile-spinner');
                if (updateProfileResponse.statusCode === 200) {
                    this.toastrService.success('User Profile Updated Successfully');
                } else {
                    this.toastrService.error(updateProfileResponse.responseMsg);
                }
            }, updateProfileError => {
                this.spinnerService.hide('editProfile-spinner');
                this.toastrService.error(updateProfileError.error.responseMsg);
            }
        );
    }
    changePassword(): void {
        if (this.changePasswordForm.invalid) {
            this.toastrService.error('Please fill the form correctly');
            return;
        }
        this.spinnerService.show('editProfile-spinner');
        this.userManagementService.changePassword(this.changePasswordForm.value).subscribe(
            changePasswordResponse => {
                this.spinnerService.hide('editProfile-spinner');
                if (changePasswordResponse.statusCode === 200) {
                    this.toastrService.success('Password Updated Successfully');
                } else {
                    this.toastrService.error(changePasswordResponse.responseMsg);
                }
            }, changePasswordError => {
                this.spinnerService.hide('editProfile-spinner');
                this.toastrService.error(changePasswordError.error.responseMsg);
            }
        );
    }
    cancel(): void {
        this.router.navigate(['/cai/home']);
    }
}
