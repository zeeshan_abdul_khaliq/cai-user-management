import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../../core-services/user-management.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {
  addOrganization: FormGroup;
  email: string;
  submitValidation: boolean;
  roles: string[];
  additionalRoles: string[];
  subscriptionTypes: string[];
  sectors: string[];
  socUsers: any[];
  showSocUsers = false;
  constructor(private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService, private userManagementService: UserManagementService) {
    this.submitValidation = false;
    this.roles = [];
    this.additionalRoles = [];
    this.subscriptionTypes = [];
    this.socUsers = [];
    this.sectors = [];
    this.addOrganization = this.formBuilder.group({
      companyName: ['', [Validators.required]],
      subscriptionType: ['', [Validators.required]],
      companyRoles: ['', [Validators.required]],
      additionalRoles: this.formBuilder.array([]),
      sector: ['', [Validators.required]],
      subscriptionStartDate: ['', [Validators.required]],
      subscriptionEndDate: ['', [Validators.required]],
      tip: [false],
      kaspersky: [false],
      ddp: [false],
      community: [false],
      infraMonitoring: [false],
      apt: [false],
      vp: [false],
      seim: [false],
      sour: [false],
      skurio: [false],
      riskXchange: [false],
      email: ['', [Validators.required, Validators.email]],
      role: ['Admin', [Validators.required]],
      socUserId: [''],
      socSupport: [''],
    });
  }
  ngOnInit(): void {
    this.lookUpServices();
  }
  get createSOCControl(): any {
    return this.addOrganization.controls;
  }
  onsocSupportChange(socEvent): any {
    if (socEvent.target.checked) {
      this.showSocUsers = true;
    } else {
      this.showSocUsers = false;
    }
  }
  onCheckboxChange(additionalRoleValue): any {
    const additionRoleControl = this.addOrganization.controls.additionalRoles as FormArray;

    if (additionalRoleValue.target.checked) {
      additionRoleControl.push(new FormControl(additionalRoleValue.target.value));
    } else {
      let additionRoleIndex = 0;
      additionRoleControl.controls.forEach((item) => {
        if (item.value === additionalRoleValue.target.value) {
          additionRoleControl.removeAt(additionRoleIndex);
          return;
        }
        additionRoleIndex++;
      });
    }
  }
  onCreateAdminSubmit(): void {
    if (this.addOrganization.invalid) {
      this.submitValidation = true;
      this.toastrService.error('Please fill the form');
      return;
    }
    const userCreateRequestDto = [{
        email: this.addOrganization.value.email,
        role: this.addOrganization.value.role,
      }];
    const createRequestListDto =  {
      userCreateRequestDtos: userCreateRequestDto
    };
    this.addOrganization.value.companyRoles = [this.addOrganization.value.companyRoles];
    this.addOrganization.value.createRequestListDto = createRequestListDto;
    this.spinnerService.show('addOrganization-spinner');
    this.userManagementService.createCompany(this.addOrganization.value).subscribe(
        updateOrganizationalUserResponse => {
          this.spinnerService.hide('addOrganization-spinner');
          this.toastrService.success('Invite has been sent to the user');
          this.router.navigate(['/admin-panel/admin']);
        }
        ,
        updateOrganizationalUserError => {
          this.spinnerService.hide('addOrganization-spinner');
          this.toastrService.error(updateOrganizationalUserError.error.responseMsg);
        });
  }
  cancel(): void {
    this.router.navigate(['/admin-panel/admin']);
  }
  lookUpServices(): void {
    this.userManagementService.lookupForSecondaryRoles().subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.roles = lookupForPrimaryRolesResponse;
          }
        });
    this.userManagementService.lookupForAdditionalRoles().subscribe(
        lookupForSecondaryRolesResponse => {
          if (lookupForSecondaryRolesResponse) {
            this.additionalRoles = lookupForSecondaryRolesResponse;
          }
        });
    this.userManagementService.lookupForSectorRoles().subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.sectors = lookupForPrimaryRolesResponse;
          }
        });
    this.userManagementService.lookupForSubscriptionType().subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.subscriptionTypes = lookupForPrimaryRolesResponse;
          }
        });
  }
  onCompanyRoleChanges(companyRole): void {
    this.userManagementService.lookupForSocUsers(companyRole.value).subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.socUsers = lookupForPrimaryRolesResponse;
          }
        });
    if (companyRole.value === 'Distributor') {
      this.additionalRoles = this.additionalRoles.filter(item => (item !== 'Distributor'));
    }else if (companyRole.value === 'Partner') {
      this.additionalRoles = this.additionalRoles.filter(item => (item !== 'Distributor' && item !== 'Partner'));
    }
  }
}
