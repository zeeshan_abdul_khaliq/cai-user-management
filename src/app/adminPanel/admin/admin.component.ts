import { Component, OnInit } from '@angular/core';
import {AdminList, ViewSocDetails} from '../../core-services/userManagements-interfaces';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../core-services/user-management.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  adminDetails: AdminList[];
  constructor(private spinnerService: NgxSpinnerService, private toastrService: ToastrService,
              private userManagementService: UserManagementService) { }

  ngOnInit(): void {
    this.adminDetails = [];
    this.getSocDetails();
  }
  getSocDetails(): void {
    this.spinnerService.show('admin-spinner');
    this.userManagementService.viewAdminDetails().subscribe(
        viewSocDetailsResponse => {
          this.spinnerService.hide('admin-spinner');
          this.adminDetails = viewSocDetailsResponse;
        }, viewSocDetailsError => {
          this.spinnerService.hide('admin-spinner');
        });
  }
}
