import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../../core-services/user-management.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-update-company',
  templateUrl: './update-company.component.html',
  styleUrls: ['./update-company.component.scss']
})
export class UpdateCompanyComponent implements OnInit {
  addOrganization: FormGroup;
  email: string;
  filter$: Observable<any>;
  orgId: string;
  submitValidation: boolean;
  roles: string[];
  additionalRoles: string[];
  subscriptionTypes: string[];
  sectors: string[];
  socUsers: any[];
  showSocUsers = false;

  constructor(private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService, private userManagementService: UserManagementService) {
    this.submitValidation = false;
    this.roles = [];
    this.additionalRoles = [];
    this.subscriptionTypes = [];
    this.socUsers = [];
    this.sectors = [];
    this.addOrganization = this.formBuilder.group({
      companyName: ['', [Validators.required]],
      subscriptionType: ['', [Validators.required]],
      companyRoles: ['', [Validators.required]],
      additionalRoles: this.formBuilder.array([]),
      sector: ['', [Validators.required]],
      subscriptionStartDate: ['', [Validators.required]],
      subscriptionEndDate: ['', [Validators.required]],
      tip: [false],
      kaspersky: [false],
      ddp: [false],
      community: [false],
      infraMonitoring: [false],
      apt: [false],
      vp: [false],
      seim: [false],
      sour: [false],
      skurio: [false],
      riskXchange: [false],
      socUserId: [''],
      socSupport: ['']
    });
  }
  ngOnInit(): void {
    this.filter$ = this.route.queryParamMap.pipe(
        map((magicLinkParam: ParamMap) => magicLinkParam.get('orgId')),
    );
    this.filter$.subscribe(param => {
      this.orgId = this.route.snapshot.queryParamMap.get('orgId');
      this.getOrganizationByOrgId();
    });
    this.lookUpServices();
  }
  getOrganizationByOrgId(): void {
    this.spinnerService.show('updateCompany-spinner');
    this.userManagementService.getOrganizationByOrgId(this.orgId).subscribe(
        getOrganizationByOrgIdResponse => {
          this.spinnerService.hide('updateCompany-spinner');
          if (getOrganizationByOrgIdResponse) {
            this.addOrganization.patchValue({
              companyName: getOrganizationByOrgIdResponse.companyName,
              subscriptionType: getOrganizationByOrgIdResponse.subscriptionType,
              companyRoles: getOrganizationByOrgIdResponse.companyBaseRole,
              additionalRoles: getOrganizationByOrgIdResponse.additionalRoles,
              sector: getOrganizationByOrgIdResponse.sector,
              subscriptionStartDate: new Date(getOrganizationByOrgIdResponse.subscriptionStartDate),
              subscriptionEndDate:  new Date(getOrganizationByOrgIdResponse.subscriptionEndDate),
              tip: getOrganizationByOrgIdResponse.tip,
              kaspersky: getOrganizationByOrgIdResponse.kaspersky,
              ddp: getOrganizationByOrgIdResponse.ddp,
              community: getOrganizationByOrgIdResponse.community,
              infraMonitoring: getOrganizationByOrgIdResponse.infraMonitoring,
              apt: getOrganizationByOrgIdResponse.apt,
              vp: getOrganizationByOrgIdResponse.vp,
              seim: getOrganizationByOrgIdResponse.seim,
              sour: getOrganizationByOrgIdResponse.sour,
              skurio: getOrganizationByOrgIdResponse.skurio,
              riskXchange: getOrganizationByOrgIdResponse.riskXchange,
              socUserId: getOrganizationByOrgIdResponse.socUserId,
            });
            this.onCompanyRoleChanges(getOrganizationByOrgIdResponse.companyBaseRole);
          }
        }, getOrganizationByOrgIdError => {
          this.spinnerService.hide('updateCompany-spinner');
        });
  }
  get createSOCControl(): any {
    return this.addOrganization.controls;
  }
  onCheckboxChange(additionalRoleValue): any {
    const additionRoleControl = this.addOrganization.controls.additionalRoles as FormArray;

    if (additionalRoleValue.target.checked) {
      additionRoleControl.push(new FormControl(additionalRoleValue.target.value));
    } else {
      let additionRoleIndex = 0;
      additionRoleControl.controls.forEach((item) => {
        if (item.value === additionalRoleValue.target.value) {
          additionRoleControl.removeAt(additionRoleIndex);
          return;
        }
        additionRoleIndex++;
      });
    }
  }
  onCreateAdminSubmit(): void {
    if (this.addOrganization.invalid) {
      this.submitValidation = true;
      this.toastrService.error('Please fill the form');
      return;
    }
    this.addOrganization.value.companyRoles = [this.addOrganization.value.companyRoles];
    this.addOrganization.value.orgId = this.orgId;
    this.spinnerService.show('updateCompany-spinner');
    this.userManagementService.updateCompany(this.addOrganization.value).subscribe(
        updateOrganizationalUserResponse => {
          this.spinnerService.hide('updateCompany-spinner');
          this.toastrService.success('Company Data Updated Successfully');
          this.router.navigate(['/admin-panel/admin']);
        }
        ,
        updateOrganizationalUserError => {
          this.spinnerService.hide('updateCompany-spinner');
          this.toastrService.error(updateOrganizationalUserError.error.responseMsg);
        });
  }
  cancel(): void {
    this.router.navigate(['/admin-panel/admin']);
  }
  lookUpServices(): void {
    this.userManagementService.lookupForSecondaryRoles().subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.roles = lookupForPrimaryRolesResponse;
          }
        });
    this.userManagementService.lookupForAdditionalRoles().subscribe(
        lookupForSecondaryRolesResponse => {
          if (lookupForSecondaryRolesResponse) {
            this.additionalRoles = lookupForSecondaryRolesResponse;
          }
        });
    this.userManagementService.lookupForSectorRoles().subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.sectors = lookupForPrimaryRolesResponse;
          }
        });
    this.userManagementService.lookupForSubscriptionType().subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.subscriptionTypes = lookupForPrimaryRolesResponse;
          }
        });
  }
  onCompanyRoleChanges(companyRole): void {
    if (!companyRole) {
      companyRole = companyRole.value;
    }
    this.userManagementService.lookupForSocUsers(companyRole).subscribe(
        lookupForPrimaryRolesResponse => {
          if (lookupForPrimaryRolesResponse) {
            this.socUsers = lookupForPrimaryRolesResponse;
          }
        });
    if (companyRole === 'Distributor') {
      this.additionalRoles = this.additionalRoles.filter(item => (item !== 'Distributor'));
    }else if (companyRole === 'Partner') {
      this.additionalRoles = this.additionalRoles.filter(item => (item !== 'Distributor' && item !== 'Partner'));
    }
  }
  onsocSupportChange(socEvent): any {
    if (socEvent.target.checked) {
      this.showSocUsers = true;
    } else {
      this.showSocUsers = false;
    }
  }
}
