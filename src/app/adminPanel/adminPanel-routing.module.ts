import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminPanelComponent} from './adminPanel.component';
import {ProfileComponent} from './profile/profile.component';
import {ManageUsersComponent} from './manage-users/manage-users.component';
import {AdminComponent} from './admin/admin.component';
import {AuthGuard} from '../core-services/auth.guard';
import { CreateUserComponent } from './manage-users/create-user/create-user.component';
import {CreateSocComponent} from './manage-users/create-soc/create-soc.component';
import {UpdateOrganizationalUserComponent} from './manage-users/update-organizationalUser/update-organizationalUser.component';
import { CreateCompanyComponent } from './admin/create-company/create-company.component';
import {UpdateCompanyComponent} from './admin/update-company/update-company.component';

const routes: Routes = [
  {
    path: '',
    component: AdminPanelComponent,
    children: [
      {
        path: '',
        component: ManageUsersComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'manageUsers',
        component: ManageUsersComponent,
        canActivate: [AuthGuard],

      },
      {
        path: 'createUser',
        component: CreateUserComponent,
        canActivate: [AuthGuard],

      },
      {
        path: 'createSoc',
        component: CreateSocComponent,
        canActivate: [AuthGuard],

      },
      {
        path: 'updateUser',
        component: UpdateOrganizationalUserComponent,
        canActivate: [AuthGuard],

      },
      {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'createCompany',
        component: CreateCompanyComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'updateCompany',
        component: UpdateCompanyComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPanelRoutingModule {}
