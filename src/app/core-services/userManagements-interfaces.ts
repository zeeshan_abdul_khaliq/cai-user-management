export interface LoginDto {
    accessToken: string;
    expiresIn: number;
    refreshExpiresIn: number;
    refreshToken: string;
    username: string;
    organizationName: string;
    role: string;
}

export interface Login {
    responseCode: number;
    responseMessage: string;
    loginDto: LoginDto;
}
export interface Profile {
    username?: string;
    email?: string;
    companyName?: string;
    phoneNumber?: string;
    roles?: string[];
    additionalRoles?: string[];
    userId?: string;
    displayName?: string;
    firstName?: string;
    lastName?: string;
    password?: string;
    enabled?: boolean;
    role?: string;
    statusCode?: number;
    responseMsg?: string;
}
export interface ActiveUser {
    emailId: string;
    firstName: string;
    lastName: string;
    role: string;
    userId?: string;
}

export interface DisabledUser {
    emailId: string;
    firstName: string;
    lastName: string;
    role: string;
}

export interface InvitedUser {
    emailId: string;
    firstName: string;
    lastName: string;
    role: string;
}

export interface OrganizationUserListDto {
    activeUsers: ActiveUser[];
    disabledUsers: DisabledUser[];
    invitedUsers: InvitedUser[];
}

export interface SocUserListDto {
    activeUsers: ActiveUser[];
    disabledUsers: DisabledUser[];
    invitedUsers: InvitedUser[];
}

export interface GetUserList {
    organizationUserListDto?: OrganizationUserListDto;
    socUserListDto?: SocUserListDto;
    soc: boolean;
}
export interface CheckMagicLinkStatus {
    statusCode: number;
    responseMsg: string;
}
export interface ViewSocDetails {
    userId: string;
    emailId: string;
    firstName: string;
    lastName: string;
    role: string;
    organizationViewDtos: OrganizationViewDto[];
}
export interface AdminList {
    id: string;
    companyName: string;
    subscriptionType: string;
    sector: string;
    subscriptionStartDate: string;
    subscriptionEndDate: string;
    companyBaseRole: string;
    organizationStatus: boolean;
    accountManager: string;
    assignedSoc: string;
}
export interface OrganizationViewDto {
    id: string;
    companyName: string;
    subscriptionType: string;
    sector: string;
    subscriptionStartDate: string;
    subscriptionEndDate: string;
    organizationStatus: boolean;
    accountManager: string;
    companyBaseRole: string;
}
export interface UserCreateRequestDto {
    email: string;
    companyName: string;
    phoneNumber: string;
    roles: string[];
    additionalRoles: string[];
}

export interface CreateUser {
    userCreateRequestDtos: UserCreateRequestDto[];
}

// Create Company
export interface UserCreateRequestDto {
    email: string;
    phoneNumber: string;
    role: string;
}

export interface CreateRequestListDto {
    userCreateRequestDtos: UserCreateRequestDto[];
}

export interface CreateCompany {
    orgId: string;
    organizationStatus: boolean;
    companyName: string;
    companyBaseRole: string;
    subscriptionType: string;
    sector: string;
    subscriptionStartDate: string;
    subscriptionEndDate: string;
    tip: boolean;
    kaspersky: boolean;
    ddp: boolean;
    community: boolean;
    infraMonitoring: boolean;
    apt: boolean;
    vp: boolean;
    seim: boolean;
    sour: boolean;
    skurio: boolean;
    riskXchange: boolean;
    companyRoles: string[];
    additionalRoles: string[];
    createRequestListDto: CreateRequestListDto;
    socUserId: string;
    socEmailId: string;
}
export interface UnassignedSocUsers {
    id: string;
    companyName: string;
}
