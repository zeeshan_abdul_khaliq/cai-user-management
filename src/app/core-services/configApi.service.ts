import {Injectable, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable()
export class ConfigApiService {
    baseUrl: string;

    constructor() {
        this.baseUrl = environment.API_URL;
    }
    /*-----------------------Adding to User Management Services------------------------------------------*/
    login(): string {
        return this.baseUrl + 'uaa-service/uaa/get-token';
    }
    updateProfile(): string {
        return this.baseUrl + 'uaa-service/uaa/update-user';
    }
    getProfile(userName): string {
        return this.baseUrl + 'uaa-service/uaa/get-user?username=' + userName;
    }
    changePassword(): string {
        return this.baseUrl + 'uaa-service/uaa/change-password';
    }
    forgotPassword(): string {
        return this.baseUrl + 'uaa-service/uaa/forgot-password';
    }
    updatePassword(): string {
        return this.baseUrl + 'uaa-service/uaa/update-password';
    }
    signUp(link): string {
        return this.baseUrl + 'uaa-service/uaa/update-profile?link=' + link;
    }
    resetLink(link): string {
        return this.baseUrl + 'uaa-service/uaa/resend-link?magicLink=' + link;
    }
    checkStatusOfLink(link): string {
        return this.baseUrl + 'uaa-service/uaa/link-status?magicLink=' + link;
    }
    getOrganizationalAndSocUserList(): string {
        return this.baseUrl + 'uaa-service/uaa/get-users-list';
    }
    getSocUserList(orgName): string {
        return this.baseUrl + 'uaa-service/uaa/get-users-list?org=' + orgName;
    }
    getOrganizationalUser(username): string {
        return this.baseUrl + 'uaa-service/uaa/get-user?username=' + username;
    }
    updateOrganizationalUser(): string {
        return this.baseUrl + 'uaa-service/uaa/update-user';
    }
    changeStatusOfUser(userId, userStatus): string {
        return this.baseUrl + 'uaa-service/uaa/enable-user?user-uuid=' + userId + '&status=' + userStatus;
    }
    addUser(): string {
        return this.baseUrl + 'uaa-service/uaa/add-user';
    }
    createCompany(): string {
        return this.baseUrl + 'uaa-service/uaa/add-organization';
    }
    updateCompany(): string {
        return this.baseUrl + 'uaa-service/uaa/update-organization';
    }
    deleteOrganizationalUser(userId): string {
        return this.baseUrl + 'uaa-service/uaa/delete-user?userId=' +  userId;
    }
    resendEmail(userId): string {
        return this.baseUrl + 'uaa-service/uaa/re-invite-user?userId=' +  userId;
    }
    viewSocDetails(userId): string {
        return this.baseUrl + 'uaa-service/uaa/get-soc-user?userId=' +  userId;
    }
    getOrganizationByOrgId(orgId): string {
        return this.baseUrl + 'uaa-service/uaa/get-organization?orgId=' +  orgId;
    }
    viewAdminDetails(): string {
        return this.baseUrl + 'uaa-service/uaa/get-organizations-list?org=' +  sessionStorage.getItem('organizationName');
    }
    lookupForPrimaryRoles(soc: string): string {
        return this.baseUrl + 'uaa-service/uaa/access-roles?soc=' + soc;
    }
    lookupForSecondaryRoles(): string {
        return this.baseUrl + 'uaa-service/uaa/company-roles';
    }
    lookupForSubscriptionType(): string {
        return this.baseUrl + 'uaa-service/uaa/subscription-types';
    }
    lookupForSectorRoles(): string {
        return this.baseUrl + 'uaa-service/uaa/sectors';
    }
    lookupForAdditionalRoles(): string {
        return this.baseUrl + 'uaa-service/uaa/additional-roles';
    }
    lookupForSocAdditionalRoles(): string {
        return this.baseUrl + 'uaa-service/uaa/soc-additional-role';
    }
    lookupForSocUsers(companyRole): string {
        return this.baseUrl + 'uaa-service/uaa/get-soc-users?companyRole=' +  companyRole;
    }
    lookupForUnAssignSocUsers(): string {
        return this.baseUrl + 'uaa-service/uaa/un-assigned-soc-organizations';
    }
    UnassignSoc(socUserId, orgId): string {
        return this.baseUrl + 'uaa-service/uaa/un-assign-soc?socUserId=' + socUserId + '&orgId=' + orgId;
    }
}
