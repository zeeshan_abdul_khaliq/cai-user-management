import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable()
export class AuthService {
  constructor(private myRoute: Router) { }
  getToken(): string {
    return sessionStorage.getItem('token');
  }
  isLoggednIn(): boolean {
    return this.getToken() !== null;
  }
}
