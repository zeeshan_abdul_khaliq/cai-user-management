import {Injectable} from '@angular/core';
import {ConfigApiService} from './configApi.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
    AdminList,
    CheckMagicLinkStatus, CreateCompany,
    CreateUser,
    GetUserList,
    Login,
    Profile, UnassignedSocUsers,
    ViewSocDetails
} from './userManagements-interfaces';
@Injectable()
export class UserManagementService {
    constructor(private httpClient: HttpClient, private configApiService: ConfigApiService) {
    }
    login(loginbody: any): Observable <Login> {
        return this.httpClient.post<Login>(this.configApiService.login(), loginbody);
    }
    getProfile(username: string): Observable <Profile> {
        return this.httpClient.get<Profile>(this.configApiService.getProfile(username));
    }
    changePassword(changePasswordForm): Observable <CheckMagicLinkStatus> {
        return this.httpClient.put<CheckMagicLinkStatus>(this.configApiService.changePassword(), changePasswordForm);
    }
    forgotPassword(forgotPasswordForm): Observable <CheckMagicLinkStatus> {
        return this.httpClient.post<CheckMagicLinkStatus>(this.configApiService.forgotPassword(), forgotPasswordForm);
    }
    updatePassword(updatePasswordForm): Observable <CheckMagicLinkStatus> {
        return this.httpClient.post<CheckMagicLinkStatus>(this.configApiService.updatePassword(), updatePasswordForm);
    }

    checkStatusOfLink(link: string): Observable <CheckMagicLinkStatus> {
        return this.httpClient.get<CheckMagicLinkStatus>(this.configApiService.checkStatusOfLink(link));
    }
    resetLink(link: string): Observable <CheckMagicLinkStatus> {
        return this.httpClient.get<CheckMagicLinkStatus>(this.configApiService.resetLink(link));
    }
    getOrganizationalAndSocUserList(): Observable <GetUserList> {
        return this.httpClient.get<GetUserList>(this.configApiService.getOrganizationalAndSocUserList());
    }
    getSocUserList(orgName): Observable <GetUserList> {
        return this.httpClient.get<GetUserList>(this.configApiService.getSocUserList(orgName));
    }
    getOrganizationalUser(username): Observable <Profile> {
        return this.httpClient.get<Profile>(this.configApiService.getOrganizationalUser(username));
    }
    deleteOrganizationalUser(userId): Observable <CheckMagicLinkStatus> {
        return this.httpClient.delete<CheckMagicLinkStatus>(this.configApiService.deleteOrganizationalUser(userId));
    }
    resendEmail(userId): Observable <CheckMagicLinkStatus> {
        return this.httpClient.get<CheckMagicLinkStatus>(this.configApiService.resendEmail(userId));
    }
    viewSocDetails(userId): Observable <ViewSocDetails> {
        return this.httpClient.get<ViewSocDetails>(this.configApiService.viewSocDetails(userId));
    }
    viewAdminDetails(): Observable <AdminList[]> {
        return this.httpClient.get<AdminList[]>(this.configApiService.viewAdminDetails());
    }
    lookupForPrimaryRoles(soc: string): Observable <string[]> {
        return this.httpClient.get<string[]>(this.configApiService.lookupForPrimaryRoles(soc));
    }
    lookupForSecondaryRoles(): Observable <string[]> {
        return this.httpClient.get<string[]>(this.configApiService.lookupForSecondaryRoles());
    }
    lookupForSubscriptionType(): Observable <string[]> {
        return this.httpClient.get<string[]>(this.configApiService.lookupForSubscriptionType());
    }
    lookupForSectorRoles(): Observable <string[]> {
        return this.httpClient.get<string[]>(this.configApiService.lookupForSectorRoles());
    }
    lookupForAdditionalRoles(): Observable <string[]> {
        return this.httpClient.get<string[]>(this.configApiService.lookupForAdditionalRoles());
    }
    lookupForSocAdditionalRoles(): Observable <string[]> {
        return this.httpClient.get<string[]>(this.configApiService.lookupForSocAdditionalRoles());
    }
    lookupForUnAssignSocUsers(): Observable <UnassignedSocUsers[]> {
        return this.httpClient.get<UnassignedSocUsers[]>(this.configApiService.lookupForUnAssignSocUsers());
    }
    lookupForSocUsers(companyRole): Observable <any> {
        return this.httpClient.get<any>(this.configApiService.lookupForSocUsers(companyRole));
    }
    updateOrganizationalUser(updateBody: Profile): Observable <Profile> {
        return this.httpClient.put<Profile>(this.configApiService.updateOrganizationalUser(), updateBody);
    }
    createOrganizationalUser(updateBody: CreateUser): Observable <CreateUser> {
        return this.httpClient.post<CreateUser>(this.configApiService.addUser(), updateBody);
    }
    changeStatusOfUser(userId, userStatus): Observable <Profile> {
        return this.httpClient.put<Profile>(this.configApiService.changeStatusOfUser(userId, userStatus), null);
    }
    addUser(addUserBody): Observable <Profile> {
        return this.httpClient.post<Profile>(this.configApiService.addUser(), addUserBody);
    }
    createCompany(createCompanyBody: CreateCompany): Observable <CreateCompany> {
        return this.httpClient.post<CreateCompany>(this.configApiService.createCompany(), createCompanyBody);
    }
    getOrganizationByOrgId(orgId: string): Observable <CreateCompany> {
        return this.httpClient.get<CreateCompany>(this.configApiService.getOrganizationByOrgId(orgId));
    }
    updateCompany(updateCompanyBody: CreateCompany): Observable <CreateCompany> {
        return this.httpClient.put<CreateCompany>(this.configApiService.updateCompany(), updateCompanyBody);
    }
    signUp(signUpBody: Profile, link: string): Observable <Profile> {
        return this.httpClient.put<Profile>(this.configApiService.signUp(link), signUpBody);
    }
    updateProfile(profileBody: Profile): Observable <Profile> {
        return this.httpClient.put<Profile>(this.configApiService.updateProfile(), profileBody);
    }
    UnassignSoc(socUserId: string, orgId: string): Observable <Profile> {
        return this.httpClient.get<Profile>(this.configApiService.UnassignSoc(socUserId, orgId));
    }
}
