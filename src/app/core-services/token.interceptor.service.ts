import {Injectable} from '@angular/core';
import {ConfigApiService} from './configApi.service';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {ToastrService} from 'ngx-toastr';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  baseURL: string;
  token: string;

  constructor(private config: ConfigApiService, private router: Router, private toastrService: ToastrService
  ) {
    this.baseURL = environment.API_URL;
  }

  intercept(httpRequest: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {
    if ((httpRequest.url.includes('uaa-service/uaa/get-token')) ||
        (httpRequest.url.includes( 'uaa-service/uaa/update-profile?link=')) ||
        (httpRequest.url.includes('uaa-service/uaa/link-status?magicLink=')) ||
        (httpRequest.url.includes('uaa-service/uaa/forgot-password')) ||
        (httpRequest.url.includes('uaa-service/uaa/update-password')) ||
        (httpRequest.url.includes('uaa-service/uaa/resend-link?magicLink=')) ) {
      httpRequest = httpRequest.clone({
        setHeaders: {
          'Content-Type': 'application/json',
        },
      });
      return httpHandler.handle(httpRequest);
    } else {
      this.token = sessionStorage.getItem('token');
      httpRequest = httpRequest.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.token,
        },
      });
      return httpHandler.handle(httpRequest).pipe(
          catchError((error: HttpErrorResponse) => {
              // handle server-side error
            if (error.status === 401) {
                this.toastrService.error('Session Expired');
                this.router.navigate(['/login']);
              }
            return throwError(error);
          }));
    }
  }
}
