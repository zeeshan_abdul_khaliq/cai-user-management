import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {AuthModule} from './auth/auth.module';
import {AdminPanelModule} from './adminPanel/adminPanel.module';
const routes: Routes = [];
@NgModule({
  imports: [RouterModule.forRoot(routes, {  }), AuthModule, AdminPanelModule],
  exports: [RouterModule],
})
export class AppRoutingModule { }
