import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../core-services/user-management.service';
import {ConfirmedValidator} from "../register/confirmed.validator";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-udpate-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  updatePasswordForm: FormGroup;
  magicLink: string;
  filter$: Observable<any>;
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private toastr: ToastrService, private userManagementService: UserManagementService,
              private route: ActivatedRoute, private formBuilder: FormBuilder) {
    this.updatePasswordForm = this.formBuilder.group({
      updatedPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: ConfirmedValidator('password', 'confirmPassword')
    });
    this.filter$ = this.route.queryParamMap.pipe(
        map((magicLinkParam: ParamMap) => magicLinkParam.get('link')),
    );
    this.filter$.subscribe(param => {
      this.magicLink = this.route.snapshot.queryParamMap.get('link');
    });
  }
  get updatePasswordFormErrorControl(): any {
    return this.updatePasswordForm.controls;
  }
  forgotPassword(): void {
    if (this.updatePasswordForm.invalid) {
      this.toastr.error( 'Please enter the email correctly');
      return;
    }
    this.updatePasswordForm.value.link = this.magicLink;
    delete this.updatePasswordForm.value.confirmPassword;
    this.spinner.show('forgotPassword-spinner');
    this.userManagementService.updatePassword(this.updatePasswordForm.value).subscribe(
        forgotPasswordResponse => {
          this.spinner.hide('forgotPassword-spinner');
          this.toastr.success(forgotPasswordResponse.responseMsg);
          this.router.navigate(['/login']);
        }, forgotPasswordError => {
          this.spinner.hide('forgotPassword-spinner');
          this.toastr.error( forgotPasswordError.error.responseMsg);
        });
  }
  ngOnInit(): void {
  }

}
