import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { AuthLayoutComponent } from './auth.component';

import {InputTextModule} from 'primeng/inputtext';
import {FormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import {ComponentsModule} from '../framework/components/components.module';
import {TabViewModule} from 'primeng/tabview';
import {TableModule} from 'primeng/table';
import {CardModule} from 'primeng/card';
import {CoreServicesModule} from '../core-services/core-services.module';
import {ToastModule} from 'primeng/toast';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from '../core-services/token.interceptor.service';
import {PasswordModule} from 'primeng/password';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {UpdatePasswordComponent} from './update-password/update-password.component';

@NgModule({
  declarations: [
    LoginComponent,
    AuthLayoutComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    AuthenticationComponent,
    UpdatePasswordComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ComponentsModule,
    CoreServicesModule,
    ConfirmDialogModule,
    InputTextModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    TabViewModule,
    CardModule,
    TableModule,
    PasswordModule,
    ToastModule,
    ToastrModule.forRoot()

  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true,
  },
      ToastrService,
  ]
})
export class AuthModule { }
