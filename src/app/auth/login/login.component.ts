import {Component, Input, OnInit} from '@angular/core';
import { FormGroup} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {UserManagementService} from '../../core-services/user-management.service';
import {Login} from '../../core-services/userManagements-interfaces';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  backendException: boolean;
  @Input() backendError: any;
  username: string;
  password: string;
  loginResponse: Login;
  constructor(private spinner: NgxSpinnerService, private router: Router, private toastrService: ToastrService,
              private userManagementService: UserManagementService) {
    this.username = '';
    this.password = '';
    this.backendException = false;
  }
  ngOnInit(): void {

  }

  submitLoginForm(loginFormValue): void {
    if (loginFormValue.username === '' || loginFormValue.password === '') {
      this.toastrService.error('Please fill the username and password.');
    }
    else {
      this.spinner.show('login-spinner');
      const loginRequest = {
        username: loginFormValue.username.trim(),
        password: loginFormValue.password.trim()
      };
      this.userManagementService.login(loginRequest).subscribe(
          loginResponse => {
            this.spinner.hide('login-spinner');
            this.loginResponse = loginResponse;
            if (this.loginResponse.responseCode === 200) {
              sessionStorage.setItem('token', this.loginResponse.loginDto.accessToken);
              sessionStorage.setItem('username', this.loginResponse.loginDto.username);
              sessionStorage.setItem('organizationName', this.loginResponse.loginDto.organizationName);
              sessionStorage.setItem('role', this.loginResponse.loginDto.role);
              this.router.navigate(['/landing']);
            } else {
              this.toastrService.error(this.loginResponse.responseMessage);
            }
          }, loginError => {
            this.spinner.hide('login-spinner');
            if (loginError.status === 0) {
              this.toastrService.error('Server is down. Please try again');
            } else {
              this.backendError = loginError.error.responseMessage;
              this.toastrService.error(this.backendError);
            }
          });
    }
  }
}
