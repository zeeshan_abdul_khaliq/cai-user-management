import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {MessageService} from 'primeng/api';
import {UserManagementService} from '../../core-services/user-management.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ConfirmedValidator} from './confirmed.validator';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    providers: [MessageService]
})
export class RegisterComponent implements OnInit {
    signUpForm: FormGroup;
    magicLink: string;
    filter$: Observable<any>;
    expiredLink: boolean;
    emailNotification: boolean;
    authenticated: boolean;
    submitValidation: boolean;
    constructor(private spinner: NgxSpinnerService, private router: Router, private messageService: MessageService,
                private toastr: ToastrService,
                private userManagementService: UserManagementService, private route: ActivatedRoute,
                private formBuilder: FormBuilder) {
        this.expiredLink = false;
        this.emailNotification = false;
        this.authenticated = false;
        this.submitValidation = false;
        this.filter$ = this.route.queryParamMap.pipe(
            map((magicLinkParam: ParamMap) => magicLinkParam.get('link')),
        );
        this.filter$.subscribe(param => {
            this.magicLink = this.route.snapshot.queryParamMap.get('link');
            this.magicLink = encodeURIComponent(this.magicLink).replace(/[!'()*]/g, c => {
                return '%' + c.charCodeAt(0).toString(16);
            });
            if (this.route.snapshot.queryParamMap.get('link')) {
                this.checkIfLinkisValid();
            } else {
                this.expiredLink = true;
            }
        });
        this.signUpForm = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
            lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
            displayName: ['', [Validators.required]],
            phoneNumber: ['', [Validators.required]],
            password: ['', [Validators.required]],
            confirmPassword: ['', [Validators.required]],
        }, {
            validator: ConfirmedValidator('password', 'confirmPassword')
        });
    }
    get signUpFormErrorControl(): any {
        return this.signUpForm.controls;
    }
    checkIfLinkisValid(): void {
        this.spinner.show('signup-spinner');
        this.userManagementService.checkStatusOfLink(this.magicLink).subscribe(
            checkStatusOfLinkResponse => {
                this.spinner.hide('signup-spinner');
                this.authenticated = true;
                this.expiredLink = false;
                this.emailNotification = false;
            }, checkStatusOfLinkError => {
                this.spinner.hide('signup-spinner');
                this.expiredLink = true;
                this.emailNotification = false;
                this.authenticated = false;
            });
    }
    sendNewMagicLink(): void {
        this.spinner.show('signup-spinner');
        this.userManagementService.resetLink(this.magicLink).subscribe(
            resetLinkResponse => {
                this.spinner.hide('signup-spinner');
                this.emailNotification = true;
                this.expiredLink = false;
                this.authenticated = false;
            }
            ,
            resetLinkError => {
                this.spinner.hide('signup-spinner');
                this.expiredLink = true;
                this.authenticated = false;
                this.emailNotification = false;
                this.toastr.error( resetLinkError.error.responseMsg);

            });
    }

    ngOnInit(): void {

    }
    onSignUpFormSubmit(): void {
        if (this.signUpForm.invalid) {
            this.submitValidation = true;
            return;
        }
        this.spinner.show('signup-spinner');
        this.userManagementService.signUp(this.signUpForm.value, this.magicLink).subscribe(
            signUpResponse => {
                this.spinner.hide('signup-spinner');
                this.toastr.success('Profile Updated Successfully');
                this.router.navigate(['/login']);
            }
            ,
            signUpError => {
                this.spinner.hide('signup-spinner');
                this.toastr.error( signUpError.error.responseMsg);

            });
    }
    routeToLogin(): void {
        this.router.navigate(['/login']);
    }
}
