import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserManagementService} from '../../core-services/user-management.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  magicLink: string;
  constructor(private spinner: NgxSpinnerService, private router: Router,
              private toastr: ToastrService, private userManagementService: UserManagementService,
              private route: ActivatedRoute, private formBuilder: FormBuilder) {
    this.forgotPasswordForm = this.formBuilder.group({
      userEmail: ['', [Validators.required, Validators.email]]
    });
  }
  get forgotPasswordFormErrorControl(): any {
    return this.forgotPasswordForm.controls;
  }
  forgotPassword(): void {
    if (this.forgotPasswordForm.invalid) {
      this.toastr.error( 'Please enter the email correctly');
      return;
    }
    this.spinner.show('forgotPassword-spinner');
    this.userManagementService.forgotPassword(this.forgotPasswordForm.value).subscribe(
        forgotPasswordResponse => {
          this.spinner.hide('forgotPassword-spinner');
          this.toastr.success('Email Sent Successfully');
          this.router.navigate(['/confirm-password']);
        }, forgotPasswordError => {
          this.spinner.hide('forgotPassword-spinner');
          this.toastr.error( forgotPasswordError.error.responseMsg);
        });
  }
  ngOnInit(): void {
  }

}
