/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  API_URL: 'https://tip-stage.cydea.tech:2096/api/',
  // DevOps can change the values here
  Stix_URL: 'http://3.135.33.205:5051/feedsip1/collections/91a7b528-80eb-42ed-a74d-c6fbd5a26116/objects/',
  Password: 'xxxxx',
  STIX_Url_Feeds: 'http://58.65.161.138:5053',
};
